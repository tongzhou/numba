import numba.ir as ir
import numba.types as types
import numba.annotations.type_annotations as type_annotations
import operator
import numba.cbelog


class Module(object):
    def __init__(self):
        self.file = open("module.cpp", "w")
        self.do_file_header()

    def do_file_header(self):
        self.do_include()

    def do_include(self):
        self.writeln("#include <vector>")
        self.writeln("#include <iostream>")
        self.writeln("#include <functional>")
        self.writeln("#include <cmath>")
        self.writeln("#include <cstdlib>")
        self.writeln('#include "cpprt.h"')
        self.writeln('')


    def writeln(self, s):
        print(s, file = self.file)

M = Module()    

class Compilation(object):
    def __init__(self):
        self.declarations = set()
        self.fpmap = {}
        self.builtin_funcmap = {}
        self.dispatch_funcmap = {}
        self.log = numba.cbelog.Logger('cbe.xml')

    def is_declared(self, name):
        if name in self.declarations:
            return True
        else:
            return False

    def add_to_declaration(self, name):
        self.declarations.add(name)

    def set_metadata(self, ir, ty_anno: type_annotations.TypeAnnotation):
        self.type_annotation = ty_anno
        self.func_ir = ir
        self.func_name = self.func_ir.func_id.func_qualname.replace('dr_main', 'main')
        self.typemap = ty_anno.typemap
        self.file = M.file

    def cleanup(self):
        pass

    def compile_standalone_func(self, ir, ty_anno: type_annotations.TypeAnnotation):
        self.set_metadata(ir, ty_anno)
        #self.do_file_header()
        self.do_function()
        self.cleanup()
        

    def is_file_open(self):
        return self.file_open        

    def do_function(self):
        self.writeln("// Function " + self.func_name)
        self.do_func_header()
        self.do_var_inits()
        self.lower_function_body()
        self.writeln("}\n")
        

    def do_func_header(self):
        ret_tystr = self.do_type_declare(self.type_annotation.return_type)
        
        func_name = self.func_name
        arglist = []
        for var in self.func_ir.arg_names:
            ty = self.get_type(var)
            tystr = self.do_type_declare(ty)

            # if isinstance(ty, types.containers.List)\
            #   or isinstance(ty, types.npytypes.Array):
            #     tystr += '&'

            s = tystr + " " + self.get_new_name(var)

            arglist.append(s)
        #print(arglist)
        s = '%s %s(%s) {' % (ret_tystr, func_name, ', '.join(arglist))    
        self.writeln(s)

    def do_var_inits(self):
        self.open_tag('varDeclares')
        for var in self.typemap:
            
            if var in self.func_ir.arg_names:
                continue
            if var.startswith('arg.'):
                continue

            self.open_tag('var')
            ty = self.get_type(var)
            tystr = self.do_type_declare(ty)

            s = tystr + " " + self.get_new_name(var) + ';'

            self.log.print(var, ty, type(ty))
            self.log.print("cpp: " + s)
            self.writeln(s)
            self.close_tag('var')
        self.close_tag('varDeclares')

    def log(self, s):
        self.log.print(s)

    def open_tag(self, name):
        self.log.open_tag(name)

    def close_tag(self, name):
        self.log.close_tag(name)

    def do_type_declare(self, ty):
        s = ''
        if type(ty) == types.scalars.Float:
            s = 'double'
        elif type(ty) == types.scalars.Integer:
            s = 'int'
        elif type(ty) == types.scalars.IntegerLiteral:
            s = 'int'  # or const int
        elif type(ty) == types.scalars.Boolean:
            s = 'bool'
        elif type(ty) == types.containers.ListIter:
            s = self.do_list_iter_type(ty)
        elif type(ty) == types.iterators.ArrayIterator:
            s = self.do_array_iter_type(ty)
        elif type(ty) == types.npytypes.Array:
            s = self.do_array_type(ty)
        elif type(ty) == types.containers.List:
            s = self.do_list_type(ty)    
        elif type(ty) == types.containers.Pair:
            s = self.do_pair_type(ty)
        elif isinstance(ty, types.misc.NoneType):
            s = "size_t"
        elif isinstance(ty, types.functions.Function):
            s = "size_t"
        elif isinstance(ty, types.functions.Dispatcher):
            s = "size_t"
        elif isinstance(ty, types.iterators.RangeType):
            s = 'std::vector<int>*'
        elif isinstance(ty, types.iterators.RangeIteratorType):
            s = 'pydd::Iter<int>*'
        else:
            print("<cbe>: unhandled type:", ty, type(ty))
            s = str(ty)
        return s

    def do_list_iter_type(self, ty: types.containers.ListIter):
        s = "pydd::Iter<%s>*" % self.do_type_declare(ty.yield_type)
        return s

    def do_array_type(self, ty: types.npytypes.Array):
        dtype = ty.dtype
        ndim = ty.ndim
        s = ''
        for i in range(ndim):
            s += 'std::vector<'
        s += self.do_type_declare(ty.dtype)    
        for i in range(ndim):
            s += '>'

        s += '*'
        
        return s

    def do_list_type(self, ty: types.containers.List):
        
        s = 'std::vector<%s>*' % (self.do_type_declare(ty.dtype))
        #s = 'std::vector<double>&'
        return s


    def do_array_iter_type(self, ty: types.iterators.ArrayIterator):
        arr_ty = ty.array_type
        s = "pydd::Iter<%s>*" % self.do_type_declare(ty.yield_type)
       
        return s

    def do_pair_type(self, ty: types.containers.Pair):
        s = 'std::pair<%s, %s>' % \
        (self.do_type_declare(ty.first_type), self.do_type_declare(ty.second_type))
        return s

    def lower_function_body(self):
        for offset, block in sorted(self.func_ir.blocks.items()):
            self.lower_block(block, offset)

            # if hasattr(inst, 'dump'):
            #     inst.dump()
            # else:
            #     inst_vars = sorted(str(v) for v in inst.list_vars())
            #     print('    %-40s %s' % (inst, inst_vars))


    def lower_block(self, B: ir.Block, offset: str):
        self.open_tag('block')
        self.log.print('label_%s: ' % offset)
        self.writeln('label_%s: ' % (offset,))

        for inst in B.body:
            self.open_tag('ins')
            self.log.print("cur inst:", inst, type(inst))

            s = ""
            if type(inst) == ir.Assign:
                s = self.lower_assign(inst)
            elif type(inst) == ir.Del:
                self.close_tag('ins')    
                continue
            elif type(inst) == ir.Jump:
                s = self.lower_jump(inst)
            elif type(inst) == ir.Return:
                s = self.lower_return(inst)
            elif type(inst) == ir.Branch:
                s = self.lower_branch(inst)
            elif type(inst) == ir.Print:
                s = self.lower_print(inst)
            elif type(inst) == ir.SetItem:
                s = self.do_setItem(inst)
            else:
                print('ignore inst:', inst, type(inst))
                self.log.print("ignore")

            self.log.print("cpp:", s)
            self.writeln(s)
            self.close_tag('ins')    
        self.close_tag('block')        
        #self.writeln('}')

    def do_setItem(self, ins: ir.SetItem):
        '''
        {'target': Var($42.26, kmeans2.py (71)), 'index': Var(j, kmeans2.py (68)), 'value': Var($42.23, kmeans2.py (71)), 'loc': Loc(filename=kmeans2.py, line=71, col=None)}
        '''
        s = '%s->at(%s) = %s;' % (self.get_new_name(ins.target),
                             self.get_new_name(ins.index),
                             self.get_new_name(ins.value))
        
        return s

    def lower_print(self, expr: ir.Print):
        s = "std::cout"
        for arg in expr.args:
            s += " << " + self.get_new_name(arg.name) + " << ' ' "; 
        s += ' << std::endl;'
        return s

    def get_new_name(self, var):
        if type(var) == ir.Var:
            return var.name.replace('$', '_').replace('.', '_')
        elif type(var) == str:
            return var.replace('$', '_').replace('.', '_')
        else:
            print("get_new_name unknown type:", type(var))
            return var.__str__()

    def lower_assign(self, inst: ir.Assign):
        if type(inst.value) == ir.Arg:
            return ""
        
        if type(inst.value) == ir.Global:
            self.do_global(inst.target, inst.value)
            return ''
            
        tystr = ''

        s = "%s%s = " % (tystr, self.do_var(inst.target))
        if type(inst.value) == ir.Var:
            s += self.do_var(inst.value)
        elif type(inst.value) == ir.Expr:
            s += self.do_expr(inst.value)
        elif type(inst.value) == ir.Const:
            s += self.do_const(inst.value)
        
        else:
            s += str(inst.value)

        return s + ';'

    def do_var(self, var: ir.Var):
        return self.get_new_name(var)

    def do_const(self, c: ir.Const):
        if c.value is None:
            return "NULL"

        return str(c.value)

    
    def do_global(self, var:ir.Var, glb: ir.Global):
        '''
        Bind the function name to the variable name
        '''
        
        ty = glb.value
        # print(vars(glb), type(ty))
        if isinstance(ty, numba.targets.registry.CPUDispatcher):
            #print(vars(glb.value), dir(glb.value), glb.value.signatures)
            self.dispatch_funcmap[var] = glb.name
        else:    
            self.builtin_funcmap[var] = glb.name
        # print('dispatch', self.dispatch_funcmap)
        # print('builtin', self.builtin_funcmap)
    
        

    def do_expr(self, expr: ir.Expr):
        #print(vars(expr))
        
        if expr.op == 'cast':
            return self.do_cast(expr)
        elif expr.op == 'inplace_binop':
            return self.do_binop(expr)
        elif expr.op == 'binop':
            return self.do_binop(expr)
        elif expr.op == 'call':
            return self.do_call(expr)
        else:
            return self.do_expr_general(expr)

    def get_ns(self):
        return 'pydd::'

    def do_call(self, expr: ir.Expr):
        
        func = expr.list_vars()[0]
        #print('call', vars(expr), vars(func))
        if self.is_builtin_fp(func):
            real_func = self.get_ns() + self.builtin_funcmap[func]
            args = ', '.join(map(lambda x: self.do_var(x), expr.list_vars()[1:]))
            s = real_func + '(' + args + ')'
            return s
        elif self.is_dispatch_fp(func):
            real_func = self.dispatch_funcmap[func]
            args = ', '.join(map(lambda x: self.do_var(x), expr.list_vars()[1:]))
            s = real_func + '(' + args + ')'
            return s

        else:
            return self.do_expr_general(expr, use_ns=False)

    def is_function_pointer(self, v):
        return v in self.fpmap

    def is_builtin_fp(self, v):
        return v in self.builtin_funcmap

    def is_dispatch_fp(self, v):
        return v in self.dispatch_funcmap

    def do_expr_general(self, expr: ir.Expr, use_ns=True):
        args = ', '.join(map(lambda x: self.do_var(x), expr.list_vars()))
        s = ''
        if use_ns:
            s  += self.get_ns()

        s += expr.op    
        if expr.op == 'build_list':
            s += '({' + args + '})'
        else:    
            s += '(' + args + ')'
        return s

    def do_cast(self, expr: ir.Expr):
        s = '('
        for i, var in enumerate(expr.list_vars()):
            if i == len(expr.list_vars()) - 1:
                s += self.do_var(var)
            else:
                s += self.do_var(var) + ','
        s += ')'
        return s

    def do_binop(self, expr: ir.Expr):
        # print(vars(expr), (expr.fn.__name__), type(expr.fn))
        # if expr.fn == operator.iadd\
        #         or expr.fn == operator.add:
        #     return '%s + %s;' % (self.get_new_name(expr.lhs), self.get_new_name(expr.rhs))


        # binop = str(expr.op)
        # if expr.fn == operator.pow:
        #     binop = 'std::pow'

        binop = self.get_ns() + expr.fn.__name__
        s = binop + '('
        for i, var in enumerate(expr.list_vars()):
            if i == len(expr.list_vars()) - 1:
                s += self.do_var(var)
            else:
                s += self.do_var(var) + ','
        s += ')'

        return s

    #def do_basic_binop(self, expr: ir.Expr):


    def do_declare(self, var):
        assert type(var) == ir.Var
        if self.is_declared(var.name):
            return ''

        ty = self.get_type(var.name)

        s = "%s " % (self.do_type_declare(ty))
        self.add_to_declaration(var.name)
        return s




    def get_type(self, name):
        return self.typemap[name]

    def lower_jump(self, I: ir.Jump):
        s = "goto label_%s;" % I.target
        return s

    def lower_return(self, I: ir.Return):
        s = "return %s;" % self.get_new_name(I.value)
        return s

    def lower_branch(self, I: ir.Branch):
        s = "if (%s) goto label_%s; else goto label_%s;" % (self.get_new_name(I.cond), I.truebr, I.falsebr)
        return s

    def writeln(self, s):
        print(s, file = self.file)

    def write(self, s):
        print(s, file=self.file, end='')
